import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import {
  Popup,
  Button,
  Icon
} from 'vant'
import 'vant/lib/index.css'

const app = createApp(App)

app.use(router)

app.use(Popup)
  .use(Icon)
  .use(Button)

app.mount('#app')