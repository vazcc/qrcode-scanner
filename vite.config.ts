import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import vueJsx from '@vitejs/plugin-vue-jsx'
import legacy from '@vitejs/plugin-legacy'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
      // es6转es5
    legacy({
      targets: ['ios >= 10', 'android >= 4.4', '> 1%']
    })
  ],
  resolve: {
    alias: { // 配置路径别名
      vue: 'vue/dist/vue.esm-bundler.js',
      '@': path.join(__dirname, 'src')
    }
  },
  server: {
    host: '0.0.0.0',
    port: 8080,
    cors: true // 默认启用并允许任何源
  }
})
